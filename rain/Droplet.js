class Droplet {
	constructor() {
		this.x = random(0, width)
		this.z = random(0, 20)
		this.y = random(-500, 0)
		this.len = map(this.z, 0, 20, 10, 20)
  	this.ySpeed = map(this.z, 0, 20, 5, 20)
    this.xSpeed = 0
	}

	rain() {
		this.y +=  this.ySpeed
    this.x += this.xSpeed
    this.ySpeed += map(this.z, 0, 20, 0, 0.2)
    this.xSpeed += random(-.1, .1)

		return this
	}

	birth() {
		const y = random(-500, -100)
		const x = random(0, width)
		const ySpeed = map(this.z, 0, 20, 5, 20)
    const xSpeed = map(this.z, 0, 20, -1, 1)
		return { y, x, ySpeed, xSpeed }
	}

	render(speed) {
		const strokeLine = map(this.z, 0, 20, 1, 3)
		const angle = map(mouseX, 0, width, -10, 10)

		strokeWeight(strokeLine)
		stroke(231, 255, 245, 150)

    //p.x + p.l * p.xs, p.y + p.l * p.ys
		line(this.x, this.y, this.x, this.y + (1 * this.ySpeed))

		if (this.y > height - 100) {
			const newPos = this.birth()
			splashes.push(new Splash(this.x, this.y + this.ySpeed))
			this.y = newPos.y
			this.x = newPos.x
      this.xSpeed = newPos.xSpeed
			this.ySpeed = speed || newPos.ySpeed
		}
		return this
	}
}
