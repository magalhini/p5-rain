class Splash {
	constructor(x, y, life = 50, r, g) {
		this.x = x
		this.y = y
		this.life = life
	}

	render() {
		const size = random(1,5)
		fill(255, this.life)
		ellipse(this.x, this.y + 20, size, size)
		this.life -= 5
	}
}
