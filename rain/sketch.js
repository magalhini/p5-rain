let drops = []
let splashes = []
let bgRain
let bgRain2

const INITIAL_DROPS = 300

function preload() {
	bgRain2 = loadSound('audio/medium-rain.mp3')
	bgRain = loadSound('audio/rain.mp3')
}

function setup() {
	createCanvas(windowWidth, windowHeight);
	angleMode(DEGREES)
	bgRain.loop()
	bgRain2.loop()

	for (let i = 0; i < INITIAL_DROPS; i++) {
		drops[i] = new Droplet()
	}
}

function draw() {
	const r = map(mouseX, 0, width, 30, 70)
	const g = map(mouseY, 0, width, 20, 50)
	const b = map(mouseY, 0, width, 20, 90)
	const speed = map(mouseX, 0, width, 4, 20)
	const volume = constrain(map(mouseX, 0, width, 0, 1), 0, 1) || .5
	bgRain2.amp(volume)
	bgRain.amp(volume / 2 + .3)

	background(r, g, b)

	if (floor(random(200)) === 10) {
		background(255,255,255)
	}

	drops
		.map(drop => drop.render(speed))
		.map(droplet => droplet.rain())

	for (let j = splashes.length - 1; j >= 0; j--) {
		splashes[j].render()
		if (splashes[j].life <= 0) {
			splashes.splice(j, 1)
		}
	}
}
